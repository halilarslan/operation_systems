1- �devlerde, �ncelikle konuyla ilgili sunum haz�rlanacak ve sunulacakt�r. Konu genel hatlar�yla sunulduktan sonra uygulamas� g�sterilecektir. 
2- Uygulamalar sanal makineler �zerinde Linux, Windows, OsX, BSD vb.. En az 2 i�letim sistemini kapsayacak �ekilde g�sterilecektir. ��letim sistemi sunumlar� bu �arta dahil de�ildir.
3- Sunum sonunda soru/cevap'larla sunum tamamlanacakt�r. 
4- Her konunun sunum s�resi 20 dk.'y� ge�meyecek �ekilde ayarlanmal�d�r.
5- Her �devi en fazla 2 ��renci birlikte yapabilir.
6- �devler Y�li�i Notunun % 50'sini (Vize %50, �dev %50) olu�turacakt�r. De�erlendirme: sunum ve konuya hakimiyet 25 puan, uygulama 50 puan, soru/cevap 25 puan
7- Sunum dosyalar� (https://bitbucket.org/halilarslan/operation_systems/branch/ogrenci) adresine .zip dosya olarak y�klenecektir. Dosya ismi konu ad� (�rn. video_audio_codec.zip) yazacak, i�eri�inde sunum dosyas� ve readme.txt i�erisinde konunun tam ad�, haz�rlayan ��renci(ler)nin ad� soyad�, okul numaras� yazacak.